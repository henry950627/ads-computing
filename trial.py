import pandas as pd
import os, sys, time
import numpy as np
import scipy as sp
import xgboost as xgb
from sklearn.cluster import KMeans
from sklearn.metrics import roc_curve, auc, f1_score
# from operator import methodcaller

def formUserAppFeat():
    path = 'pre/user_app_feat.csv'
    if not os.path.exists(path):
        user = pd.read_csv('pre/user.csv')
        userInstalledApps = pd.read_csv('pre/user_installedapps.csv')
        app = pd.read_csv('pre/app_categories.csv')
        userInstalledAppCates = pd.merge(userInstalledApps, app, how="inner", on="appID")
        userInstalledAppCates = userInstalledAppCates.loc[:, ['userID','appCategory']]
        userInstalledAppCates = userInstalledAppCates.drop_duplicates()
        print(userInstalledAppCates.shape)

        appCates = np.unique(app.as_matrix()[:,1])
        for cate in appCates:
            user['app{}'.format(cate)] = 0
        print(user.head())

        j = 0
        old = time.time()
        for i in xrange(userInstalledAppCates.shape[0]):
            if (i+1) % 50000 == 0:
                new = time.time()
                print("loop {} done, consuming {}secs".format(i+1, new-old))
                old = new
            currentUser = userInstalledAppCates.iloc[i, 0]
            currentAppCate = userInstalledAppCates.iloc[i, 1]
            user.loc[currentUser-1, 'app{}'.format(currentAppCate)] = 1
        user.to_csv(path, index=False)
    else:
        user = pd.read_csv(path)
    return user


def formUserLocFeat():
    def zeros2str(n):
        if n==0:
            return "0000"
        else:
            return str(n)
    # user = pd.read_csv('pre/user.csv')
    user = pd.read_csv('pre/user_app_feat.csv')
    # res = np.unique(x.as_matrix[:, -1])
    # home = np.unique(x.as_matrix[:, -2])
    home = user.pop('hometown').tolist()
    res = user.pop('residence').tolist()
    home = map(zeros2str, home)
    res = map(zeros2str, res)
    homeProv = map(int, map(lambda s: s[:-2], home))
    homeCity = map(int, map(lambda s: s[-2:], home))
    resProv = map(int, map(lambda s: s[:-2], res))
    resCity = map(int, map(lambda s: s[-2:], res))
    # print(len(homeProv))
    user.insert(6, 'homeProvince', np.asarray(homeProv))
    user.insert(7, 'homeCity', np.asarray(homeCity))
    user.insert(8, 'resProvince', np.asarray(resProv))
    user.insert(9, 'resCity', np.asarray(resCity))
    return user
    
def formUserFeat(path):
    if not os.path.exists(path):
        user = formUserAppFeat()
        user = formUserLocFeat()
        user.to_csv(path, index=False)
    else:
        user = pd.read_csv(path)
    return user

def formDataset(path):
    if not os.path.exists(path):
        log = pd.read_csv('pre/train.csv')
        logIgnoreTime = log.loc[:, ['label', 'creativeID', 'userID', 'positionID', 'connectionType', 'telecomsOperator']]
        logIgnoreTime.insert(0, 'ind',np.arange(logIgnoreTime.shape[0]))

        # user = pd.read_csv('pre/user.csv')
        user = formUserFeat('pre/user_feat.csv')
        ad = pd.read_csv('pre/ad.csv')
        position = pd.read_csv('pre/position.csv')
        app = pd.read_csv('pre/app_categories.csv')

        data = pd.merge(logIgnoreTime, user, how="inner", on="userID")
        tmp = pd.merge(ad, app, how="inner", on="appID")
        data = pd.merge(data, tmp, how="inner", on="creativeID")
        data = pd.merge(data, position, how="inner", on="positionID")

        
        data.sort_values(by='ind', inplace=True)
        tobeDropped = ['ind', 'userID', 'adID', 'positionID', 'appID']
        for col in tobeDropped:
            data.drop(col, axis=1, inplace=True)
        # data = data.sample(n=data.shape[0])

        data.to_csv(path, index=False)
        return data
    else:
        return pd.read_csv(path)

def removeLastFewDays(data):
    pass

def cluster(data, n_clusters, max_iter=3000, n_init=10):
    machine = KMeans(n_clusters=n_clusters, max_iter=max_iter, n_init=n_init, verbose=1)
    result = machine.fit(data.as_matrix())
    return result 

def logloss(pred, act):
    epsilon = 1e-6
    pred = np.maximum(epsilon, pred)
    pred = np.minimum(1-epsilon, pred)
    ll = np.sum(act*np.log(pred) + (1-act)*np.log(1-pred))
    ll = ll * -1.0/act.shape[0]
    return ll

def accuracy(pred, act):
    res = (pred > 0.5).astype(np.int)
    act = act.astype(np.int)
    return np.sum((res==act).astype(np.int)).astype(np.float)/len(act)
    


# userAppFeat(path)
data = formDataset("train_joined.csv")
converted = data[data.label==1].iloc[:,1:]
notConverted = data[data.label==0].iloc[:,1:]
size = converted.shape[0]

dataOrigin = data
label = np.squeeze(data.iloc[:,:1].values)
data = data.iloc[:,1:]
split = 3200000
dtrain = xgb.DMatrix(data.iloc[:split, :], label[:split])
dtest = xgb.DMatrix(data.iloc[split:, :], label[split:])

dataOriginTest = dataOrigin.iloc[split:, :]
dataOriginTestPos = dataOriginTest[dataOriginTest.label==1]
dataOriginTestNeg = dataOriginTest[dataOriginTest.label==0]
labelPos = np.squeeze(dataOriginTestPos.iloc[:,:1].values)
dataOriginTestPos = dataOriginTestPos.iloc[:,1:]
labelNeg = np.squeeze(dataOriginTestNeg.iloc[:,:1].values)
dataOriginTestNeg = dataOriginTestNeg.iloc[:,1:]

dtestPos = xgb.DMatrix(dataOriginTestPos, labelPos)
dtestNeg = xgb.DMatrix(dataOriginTestNeg, labelNeg)
params = {
    'objective': 'binary:logistic',
    'max_depth': 8,
    'eta': 0.4,
    #'scale_pos_weight': 1, # Useful for unbalanced classes, a typical value: sum(negative classes)/sum(positive classes)
    'subsample': 1, # subsample ratio of the training instance. Setting it to 0.5 means that XGBoost randomly collected half of the data instances to grow trees and this will prevent overfitting.
    'max_delta_step': 0 # Maximum delta step we allow each tree's weight estimation to be. If the value is set to 0, it means there is no constraint. If it is set to a positive value, it can help making the update step more conservative. Usually this parameter is not needed, but it might help in logistic regression when class is extremely imbalanced. Set it to value of 1-10 might help control the update

}
watchlist = [(dtest, 'eval'), (dtrain, 'train')]
nround = 10
bst = xgb.train(params, dtrain, nround, watchlist)
preds = bst.predict(dtest)
labels = dtest.get_label()
np.save("preds.npy",preds)
pos_preds = bst.predict(dtestPos)
neg_preds = bst.predict(dtestNeg)
pos_labels = dtestPos.get_label()
neg_labels = dtestNeg.get_label()
np.set_printoptions(threshold='nan')
fpr, tpr, thresholds = roc_curve(labels, preds, pos_label=1)
with open("result.log","a") as f:
    f.write(str(params) + '\n')
    f.write("\tlog loss: {}\n".format(logloss(preds, labels)))
    f.write("\taccuracy positive: {}\n".format(accuracy(pos_preds, pos_labels)))
    f.write("\taccuracy negative: {}\n".format(accuracy(neg_preds, neg_labels)))
    f.write("\tAUC: {}\n".format(auc(fpr, tpr)))
    f.write("--------------------\n")
print("log loss: {}".format(logloss(preds, labels)))
print("accuracy positive: {}".format(accuracy(pos_preds, pos_labels)))
print("accuracy negative: {}".format(accuracy(neg_preds, neg_labels)))
print(auc(fpr, tpr))
# print(f1_score(labels, preds))
# print(pos_preds.shape)

# kmeans = cluster(notConverted, 8, max_iter=3, n_init=1)
# np.save("clusters.npy", kmeans.cluster_centers_)
