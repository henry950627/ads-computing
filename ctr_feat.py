import pandas as pd
import collections, itertools
import os, time, csv, hashlib
import numpy as np

def merge(to, mode="train"):

    if not os.path.exists(to):
        log = pd.read_csv('pre/train.csv') if mode=="train" else pd.read_csv("pre/test.csv")
        # logIgnoreTime = log.drop("clickTime",axis=1)
        logIgnoreTime = log
        if "conversionTime" in logIgnoreTime.keys():
            logIgnoreTime = logIgnoreTime.drop("conversionTime",axis=1)
        if "instanceID" not in logIgnoreTime.keys():
            logIgnoreTime.insert(0, 'instanceID',np.arange(logIgnoreTime.shape[0]))
        def raw2hour(s):
            return int(str(s)[:4])

        hours = logIgnoreTime.clickTime.map(raw2hour)
        logIgnoreTime['hour'] = hours
        
        user = pd.read_csv('pre/user.csv')
        ad = pd.read_csv('pre/ad.csv')
        position = pd.read_csv('pre/position.csv')
        app = pd.read_csv('pre/app_categories.csv')

        data = pd.merge(logIgnoreTime, user, how="inner", on="userID")
        tmp = pd.merge(ad, app, how="inner", on="appID")
        data = pd.merge(data, tmp, how="inner", on="creativeID")
        data = pd.merge(data, position, how="inner", on="positionID")

        
        data.sort_values(by='instanceID', inplace=True)
        tobeDropped = ['instanceID'] if mode=="train" else []
        for col in tobeDropped:
            data.drop(col, axis=1, inplace=True)
        data.to_csv(to, index=False)

def genUserHistory(to, mode="train"):

    if mode == "train":
        _end = 2834728 # 28th start
        log = pd.read_csv("train_joined.csv")
        reader = csv.DictReader(open('train_joined.csv'))
    elif mode == "all":
        log = pd.read_csv("train_joined.csv")
        _end = log.shape[0]
        log_test = pd.read_csv("test_joined.csv")
        log_test.drop("instanceID", axis=1, inplace=True)
        log = log.append(log_test, ignore_index=True)
        log.to_csv("all_joined.csv", index=False)
        reader = csv.DictReader(open('all_joined.csv'))

    user_history = collections.defaultdict(lambda: {'history':'', 'buffer':'', 'prev_hour':''})
    # creative_history = collections.defaultdict(lambda: {'history':'', 'buffer':'', 'prev_hour':''})
    # app_history = collections.defaultdict(lambda: {'history':'', 'buffer':'', 'prev_hour':''})
    tmp_user = ['' for _ in xrange(log.shape[0])]
    # tmp_creative = ['' for _ in xrange(log.shape[0])]
    # tmp_app = ['' for _ in xrange(log.shape[0])]
    start = time.time()
    is_train = True
    for i, row in enumerate(reader):
        
        user = row['userID']
        if user_history[user]['prev_hour'] != row['clickTime'][:4]:
            user_history[user]['history'] += user_history[user]['buffer']
            user_history[user]['buffer'] = ''
            user_history[user]['prev_hour'] = row['clickTime'][:4]
        tmp_user[i] = user_history[user]['history']

        # creative = row['creativeID']
        # if creative_history[creative]['prev_hour'] != row['clickTime'][:4]:
        #     creative_history[creative]['history'] += creative_history[creative]['buffer']
        #     creative_history[creative]['buffer'] = ''
        #     creative_history[creative]['prev_hour'] = row['clickTime'][:4]
        # tmp_creative[i] = creative_history[creative]['history']

        # app = row['appID']
        # if app_history[app]['prev_hour'] != row['clickTime'][:4]:
        #     app_history[app]['history'] += app_history[app]['buffer']
        #     app_history[app]['buffer'] = ''
        #     app_history[app]['prev_hour'] = row['clickTime'][:4]
        # tmp_app[i] = app_history[app]['history']

        if i == _end:
            is_train = False
        if is_train:
            user_history[user]['buffer'] += row['label']
            # creative_history[creative]['buffer'] += row['label']
            # app_history[app]['buffer'] += row['label']
        if (i+1) % 100000 == 0:
            print('{0:6.0f} {1} iters\n'.format(time.time()-start,i+1))
    log['user_click_history'] = tmp_user
    # log['creative_click_history'] = tmp_creative
    # log['app_click_history'] = tmp_app
    log.to_csv(to, index=False)

def genHistAppearance(src, to, mode='train'):

    log = pd.read_csv(src, dtype={'user_click_history': str})
    log.fillna('', inplace=True)
    df = log[log.duplicated(['userID','appID'], keep=False)].loc[:,['userID','appID','label']]
    df2 = log[log.duplicated(['userID','appID'])]
    df3 = log[log.duplicated(['userID','creativeID'])]
    if mode == 'train':
        till = 2834728
    elif mode == 'all':
        till = np.inf

    D = {}
    for idx, row in df.iterrows():
        d = row.to_dict()
        tmp = '{}-{}'.format(str(d['userID']), str(d['appID']))
        if not D.has_key(tmp):
            if str(d['label'])=='1' and idx<till:
                D[tmp] = ['ok']
            else:
                continue
        else:
            D[tmp].append(idx)

    res = reduce(lambda x, y: x[1:]+y[1:], D.values(), [])
    log.loc[df2.index, 'app_clicked'] = 1
    log.fillna(0, inplace=True)
    log.loc[df3.index, 'creative_clicked'] = 1
    log.fillna(0, inplace=True)
    log.loc[res, 'converted'] = 1
    log.fillna(0, inplace=True)

    log.to_csv(to, index=False)

def historyConvertStats(name, mode='train'):
    if mode == "train":
        _end = 2834728 # 28th start
        fname = 'train_joined1.csv'
    elif mode == "all":
        _end = 3749528
        fname = 'all_joined1.csv'
    reader = csv.DictReader(open(fname, 'r'))
    userDict = collections.defaultdict(lambda: {'click': 0, 'converted': 0})
    creativeDict = collections.defaultdict(lambda: {'click': 0, 'converted': 0})
    appDict = collections.defaultdict(lambda: {'click': 0, 'converted': 0})
    is_train = True
    with open(name, 'w') as f:
        header = open(fname, 'r').readline()[:-1]
        f.write('{},{},{},{}\n'.format(header, 'userCR','creativeCR','appCR'))
        header_list = header.split(',')
        for i, row in enumerate(reader):
            newRow = [row[k] for k in header_list]
            user = row['userID']
            creative = row['creativeID']
            app = row['appID']
            if userDict[user]['click'] == 0:
                newRow.append(str(0))
            else:
                newRow.append(str(float(userDict[user]['converted'])/userDict[user]['click']))
            if creativeDict[creative]['click'] == 0:
                newRow.append(str(0))
            else:
                newRow.append(str(float(creativeDict[creative]['converted'])/creativeDict[creative]['click']))
            if appDict[app]['click'] == 0:
                newRow.append(str(0))
            else:
                newRow.append(str(float(appDict[app]['converted'])/appDict[app]['click']))
            f.write('{}\n'.format(','.join(newRow)))
            if (i+1) % 100000 == 0:
                print('iters {} done.'.format(i+1))
            if i == _end:
                is_train = False

            if is_train:
                userDict[user]['click'] += 1
                creativeDict[creative]['click'] += 1
                appDict[app]['click'] += 1

                if int(row['label']) == 1:
                    userDict[user]['converted'] += 1
                    creativeDict[creative]['converted'] += 1
                    appDict[app]['converted'] += 1






def computeAppearRate():
    userDict = {}
    with open('history_click_rate.csv','w') as f:
        s = 0
        for i, row in enumerate(csv.DictReader(open('train_joined_lambda.csv','r'))):
            newRow = [row['userID']]
            # if row['userID'] not in userDict.keys():
            if not userDict.has_key(row['userID']):
                newRow.append('-1')
                userDict[row['userID']] = 1
                s += 1
            else:
                newRow.append(str(float(userDict[row['userID']])/s))
                userDict[row['userID']] += 1
                s += 1
            newRow.append(row['user_click_history'])
            f.write('{}\n'.format(','.join(newRow)))
            if (i+1) % 10000 == 0:
                print('iters {} done.'.format(i+1))
    

def genStats(src, to):

    # log = pd.read_csv("train_joined3.csv", dtype={'user_click_history':str})
    # log = pd.read_csv("all_joined3.csv", dtype={'user_click_history':str})
    log = pd.read_csv(src, dtype={'user_click_history':str})
    log.drop('age', axis=1, inplace=True)
    log.fillna('', inplace=True)
    feats = ['creativeID','userID','positionID','connectionType','telecomsOperator','gender','education','marriageStatus','haveBaby','hometown','residence','adID','camgaignID','advertiserID','appID','appPlatform','appCategory','sitesetID','positionType']

    log['{}-{}'.format('userID', 'hour')] = log['userID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    log['{}-{}'.format('creativeID', 'hour')] = log['creativeID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    log['{}-{}'.format('appID', 'hour')] = log['appID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    
    for f in feats + ['userID-hour', 'creativeID-hour', 'appID-hour']:
        z = log[f].value_counts()
        z = z.to_frame()
        z.reset_index(level=0, inplace=True)
        z.columns.values[0] = f
        z.columns.values[1] = f + "_count"
        log = pd.merge(log, z, on=f, how="left")
    
    log.fillna(0, inplace=True)
    log.to_csv(to, index=False)

def formStatsCross(name, mode="train"):

    if mode == "train":
        log = pd.read_csv("train_joined1.csv", dtype={'user_click_history':str})
    elif mode == "all":
        log = pd.read_csv("all_joined1.csv", dtype={'user_click_history':str})
    log.drop('age', axis=1, inplace=True)
    log.fillna('', inplace=True)
    feats = ['creativeID','userID','positionID','connectionType','telecomsOperator','gender','education','marriageStatus','haveBaby','hometown','residence','adID','camgaignID','advertiserID','appID','appPlatform','appCategory','sitesetID','positionType']

    log['{}-{}'.format('userID', 'hour')] = log['userID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    log['{}-{}'.format('creativeID', 'hour')] = log['creativeID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    log['{}-{}'.format('appID', 'hour')] = log['appID'].map(lambda x: str(x)+'-') + log['hour'].map(str)
    
    for f in feats + ['userID-hour', 'creativeID-hour', 'appID-hour']:
        z = log[f].value_counts()
        z = z.to_frame()
        z.reset_index(level=0, inplace=True)
        z.columns.values[0] = f
        z.columns.values[1] = f + "_count"
        log = pd.merge(log, z, on=f, how="left")

    comb = list(itertools.combinations(feats, 2))
    for c in comb:
        log["{}-{}".format(c[0],c[1])] = log[c[0]].map(lambda x: str(x)+'-') + log[c[1]].map(str)
        print("{} done.".format(c))
    for c in comb:
        z = log["{}-{}".format(c[0],c[1])].value_counts()
        z = z.to_frame()
        z.reset_index(level=0, inplace=True)
        z.columns.values[0] = "{}-{}".format(c[0],c[1])
        z.columns.values[1] = "{}-{}_count".format(c[0],c[1])
        log = pd.merge(log, z, on="{}-{}".format(c[0],c[1]), how="left")
        print("{} done.".format(c))
    
    log.fillna(0, inplace=True)
    log.to_csv(name, index=False)

    
    
def hashstr(s):
    return str(int(hashlib.md5(s.encode('utf8')).hexdigest(), 16)%(1000000-1)+1)

def hashConvert(src, to):
    fields_ID = ['creativeID','userID','positionID','hometown','residence','adID','camgaignID','advertiserID','appID','appCategory']
    fields_nonID = ['connectionType','telecomsOperator','gender','education','marriageStatus','haveBaby', 'sitesetID','appPlatform','positionType']
    fields = [f + '_count' for f in fields_ID]
    fname = src
    # fname = 'train_joined_lambda.csv'
    # fname = 'train_joined_gamma.csv'
    # fname = 'all_joined_lambda.csv'
    # fname = 'all_joined_gamma.csv'
    special_treat = ['userID_count','creativeID_count','userID-hour_count','creativeID-hour_count', 'appID-hour_count','user_click_history']
    # numeric = ['userCR','creativeCR','appCR']
    hist = ['app_clicked', 'creative_clicked', 'converted']
    fields = list(set(fields) - set(special_treat))
    with open(to, 'w') as f:
        header = ['ind','label']
        header += fields_nonID + fields_ID
        header.append('hour')
        # header.extend(fields)
        header.extend(special_treat)
        # header.extend(numeric)
        header.extend(hist)
        f.write(','.join(header) + '\n')
        for i, row in enumerate(csv.DictReader(open(fname,'r'))):

            feats = []
            for field in fields_nonID:
                feats.append(row[field])
            for field in fields_ID:
                feats.append(hashstr(field+'-'+row[field]))
            # for field in fields:
            #     if int(row[field]) > 20000:
            #         feats.append(hashstr(field[:-6] + '-a'))
            #     else:
            #         feats.append(hashstr(field[:-6] + '-less-' + row[field]))
            feats.append(hashstr('hour-'+row['hour']))

            if int(row['userID_count']) > 50:
                feats.append(hashstr('userID-'+row['userID']))
            else:
                feats.append(hashstr('userID-less-'+row['userID_count']))

            if int(row['creativeID_count']) > 10000:
                feats.append(hashstr('creativeID-'+row['creativeID']))
            else:
                feats.append(hashstr('creativeID-less-'+row['creativeID_count']))

            if int(row['userID-hour_count']) > 5:
                feats.append(hashstr('userID-hour-a'))
            else:
                feats.append(hashstr('userID-hour-'+row['userID-hour_count']))

            if int(row['creativeID-hour_count']) > 30:
                feats.append(hashstr('creativeID-hour-a'))
            else:
                feats.append(hashstr('creativeID-hour-'+row['creativeID-hour_count']))
            if int(row['appID-hour_count']) > 30:
                feats.append(hashstr('appID-hour-a'))
            else:
                feats.append(hashstr('appID-hour-'+row['appID-hour_count']))

            if int(row['userID_count']) > 30:
                feats.append(hashstr('user_click_history-'+row['userID_count']))
            else:
                feats.append(hashstr('user_click_history-'+row['userID_count']+'-'+row['user_click_history']))

            # feats.extend([row[k] for k in numeric])
            feats.extend([str(int(float(row[k]))) for k in hist])
            f.write('{},{},{}\n'.format(i, row['label'], ','.join(feats)))
            if (i+1) % 100000 == 0:
                print("iters {}".format(i+1))

def hashConvertCross(name, mode="train"):
    fields_ID = ['creativeID','userID','positionID','hometown','residence','adID','camgaignID','advertiserID','appID','appPlatform','appCategory','sitesetID','positionType']
    fields_nonID = ['connectionType','telecomsOperator','gender','education','marriageStatus','haveBaby']
    if mode == "train":
        # fname = 'train_joined_lambda.csv'
        fname = 'train_joined_gamma.csv'
    elif mode == "all":
        # fname = 'all_joined_lambda.csv'
        fname = 'all_joined_gamma.csv'
    columns = open(fname,'r').readline()[:-1].split(',')
    special_treat = ['userID_count','creativeID_count','userID-hour_count','creativeID-hour_count','user_click_history']
    for s in special_treat:
        columns.remove(s)
    columns.remove('appID-hour_count')
    columns = [col for col in columns if 'count' in col and '-' in col]
    print('\t'.join(columns))
    with open(name, 'w') as f:
        header = ['ind','label']
        header += fields_nonID + fields_ID
        header.append('hour')
        header.extend(columns)
        header.extend(special_treat)
        f.write(','.join(header) + '\n')
        for i, row in enumerate(csv.DictReader(open(fname,'r'))):

            feats = []
            for field in fields_nonID:
                feats.append(row[field])
            for field in fields_ID:
                feats.append(hashstr(field+'-'+row[field]))
            feats.append(hashstr('hour-'+row['hour']))

            for count_col in columns:
                if int(row[count_col]) > 1000:
                    tmp = row[count_col[:-6]]
                    feats.append(hashstr(count_col+'-'+tmp))
                else:
                    feats.append(hashstr(count_col+'-'+row[count_col]))

            if int(row['userID_count']) > 50:
                feats.append(hashstr('userID-'+row['userID']))
            else:
                feats.append(hashstr('userID-less-'+row['userID_count']))

            if int(row['creativeID_count']) > 10000:
                feats.append(hashstr('creativeID-'+row['creativeID']))
            else:
                feats.append(hashstr('creativeID-less-'+row['creativeID_count']))

            if int(row['userID-hour_count']) > 5:
                feats.append(hashstr('userID-hour-a'))
            else:
                feats.append(hashstr('userID-hour-'+row['userID-hour_count']))

            if int(row['creativeID-hour_count']) > 30:
                feats.append(hashstr('creativeID-hour-a'))
            else:
                feats.append(hashstr('creativeID-hour-'+row['creativeID-hour_count']))

            if int(row['userID_count']) > 30:
                feats.append(hashstr('user_click_history-'+row['userID_count']))
            else:
                feats.append(hashstr('user_click_history-'+row['userID_count']+'-'+row['user_click_history']))
            f.write('{},{},{}\n'.format(i, row['label'], ','.join(feats)))
            if (i+1) % 100000 == 0:
                print("iters {}".format(i+1))





