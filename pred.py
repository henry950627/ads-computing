import pandas as pd
x = pd.read_csv("pred.ffm", header=None)
x.columns = pd.Index(['prob'])
x.reset_index(level=0, inplace=True)
x.columns.values[0] = 'instanceID'
x['instanceID'] += 1
print(x.prob.mean())
x.to_csv('submission.csv', index=False)

