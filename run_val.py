import ctr_feat
import pandas as pd
from format import FFMFormatPandas as F

""" Merge basic table
"""
ctr_feat.merge('train_joined.csv')
ctr_feat.merge('test_joined.csv', mode='test')

""" Generate user clicks history log
"""
ctr_feat.genUserHistory(to='train_joined1.csv', mode='train')

""" Generate tags indicating historical appearance of log items(Magic Feature)
"""
ctr_feat.genHistAppearance(src='train_joined1.csv', to='train_joined1.csv', mode='train')

""" Generate counting statistics
"""
ctr_feat.genStats(src='train_joined1.csv', to='train_joined_alpha.csv')

""" Hash trick
"""
ctr_feat.hashConvert(src='train_joined_alpha.csv', to='train_joined_hash.csv')

f = F()
data = pd.read_csv('train_joined_hash.csv', dtype=str)
data.drop('ind', axis=1, inplace=True)
data.drop(['hour','creativeID'], axis=1, inplace=True)
hashedData = f.fit_transform(df=data, y='label')

_28th_start = 2834728
_29th_end = 3435131

hashedData.iloc[:_28th_start].to_csv('train.ffm', index=False)
hashedData.iloc[_28th_start:_29th_end].to_csv('val.ffm', index=False)

""" Then called `./ffm-train -t 43 -k 5 -r 0.05 -p val.ffm train.ffm val.model` for training and validation.
"""
