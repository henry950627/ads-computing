import ctr_feat
import pandas as pd
from format import FFMFormatPandas as F

""" Merge basic table
"""
ctr_feat.merge('train_joined.csv')
ctr_feat.merge('test_joined.csv', mode='test')

""" Generate user clicks history log
"""
ctr_feat.genUserHistory(to='all_joined1.csv', mode='all')

""" Generate tags indicating historical appearance of log items(Magic Feature)
"""
ctr_feat.genHistAppearance(src='all_joined1.csv', to='all_joined1.csv', mode='all')

""" Generate counting statistics
"""
ctr_feat.genStats(src='all_joined1.csv', to='all_joined_alpha.csv')

""" Hash trick
"""
ctr_feat.hashConvert(src='all_joined_alpha.csv', to='all_joined_hash.csv')

""" Convert to ffm format.
"""
f = F()
data = pd.read_csv('all_joined_hash.csv', dtype=str)
data.drop('ind', axis=1, inplace=True)
data.drop(['hour','creativeID'], axis=1, inplace=True)
hashedData = f.fit_transform(df=data, y='label')

_30th_end = 3749528

hashedData.iloc[:_30th_end].to_csv('all_train.ffm', index=False)
hashedData.iloc[_30th_end:].to_csv('all_test.ffm', index=False)

""" Then called:
        `./ffm-train -t 43 -k 5 -r 0.05 all_train.ffm all.model` and
        `./ffm-predict all_test.ffm all.model pred.ffm` and
        `python pred.py`
    to generate submission.csv
"""
