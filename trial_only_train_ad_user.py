import trial
import xgboost as xgb
import pandas as pd
import numpy as np
import os
import trial


data = trial.formDataset("train_joined.csv")
tobeDropped = ['app0','app1','app2','app101','app103','app104','app105','app106','app108','app109','app110','app201','app203','app204','app209','app210','app211','app301','app303','app401','app402','app403','app405','app406','app407','app408','app409','app503']
for drop in tobeDropped:
    data.drop(drop,axis=1,inplace=True)

till = -1 #3113362
data = data.iloc[:till, :]
split = 3113362 #2000000
label = np.squeeze(data.iloc[:, :1].values)
data = data.iloc[:, 1:]
dtrain = xgb.DMatrix(data.iloc[:split,:], label[:split]) 
dtest = xgb.DMatrix(data.iloc[split:,:], label[split:]) 
data = None
params = {
    'objective': 'binary:logistic',
    'eval_metric': 'logloss',
    'max_depth': 11,
    'eta': 0.1,
    'colsample_bytree': 0.8,
    'min_child_weight': 3,
    #'scale_pos_weight': 1, # Useful for unbalanced classes, a typical value: sum(negative classes)/sum(positive classes)
    'subsample': 1, # subsample ratio of the training instance. Setting it to 0.5 means that XGBoost randomly collected half of the data instances to grow trees and this will prevent overfitting.
    'max_delta_step': 0, # Maximum delta step we allow each tree's weight estimation to be. If the value is set to 0, it means there is no constraint. If it is set to a positive value, it can help making the update step more conservative. Usually this parameter is not needed, but it might help in logistic regression when class is extremely imbalanced. Set it to value of 1-10 might help control the update
    'alpha': 4, # L1 regularization term on weights, increase this value will make model more conservative
    'gamma': 1, # minimum loss reduction required to make a further partition on a leaf node of the tree. The larger, the more conservative the algorithm will be.
    'lambda': 2 # L2 regularization term on weights, increase this value will make model more conservative.
}
watchlist = [(dtest, 'eval'), (dtrain, 'train')]
nround = 173
bst2 = xgb.train(params, dtrain, nround, watchlist)
preds = bst2.predict(dtest)
bst2.save_model('models/baseline_ad_user.model')
