# Advertisiment Computing

> group: 333B

## Requires

### [data](http://spabigdata-1253211098.file.myqcloud.com/pre.zip)

### [xgboost library](https://github.com/dmlc/xgboost)

+ Install
    + From Github
    
        ```bash
        git clone https://github.com/dmlc/xgboost.git
        cd xgboost/dmlc-core
        git clone https://github.com/dmlc/dmlc-core.git
        cd ../rabit
        git clone https://github.com/dmlc/rabit.git
        cd ..
        make -j8
        cd python-package
        python setup.py install
        ```
    + From this repo

        ```bash
        cd xgboost
        make -j8
        cd python-package
        python setup.py install
        ```

+ Usage

```python
import xgboost as xgb
```

+ [Documents](https://github.com/tqchen/xgboost/tree/master/doc)
+ [Python Guide](https://github.com/dmlc/xgboost/tree/master/demo/guide-python)
