import xgboost as xgb
import trial
import pandas as pd
bst = xgb.Booster(model_file="models/baseline_ad_user.model")
data = trial.formDataset("test_joined.csv",mode="test")
tobeDropped = ['app0','app1','app2','app101','app103','app104','app105','app106','app108','app109','app110','app201','app203','app204','app209','app210','app211','app301','app303','app401','app402','app403','app405','app406','app407','app408','app409','app503']
for drop in tobeDropped:
    data.drop(drop,axis=1,inplace=True)
# label = np.squeeze(data.iloc[:,:1].values)
data1 = data.iloc[:,2:] # test.csv has an extra columns called "instanceID"
dtest = xgb.DMatrix(data1)
preds = bst.predict(dtest)

df = pd.DataFrame({
    'instanceID': data.instanceID,
    'prob': preds
})
df.to_csv('submission.csv', index=False)
    
