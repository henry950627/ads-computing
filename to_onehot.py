import trial
import pandas as pd
import numpy as np
data = trial.formDataset("train_joined.csv")
dummies = ['appPlatform','connectionType','telecomsOperator','sitesetID','positionType','gender','education','marriageStatus','haveBaby']
for dummy in dummies:
    one_hot = pd.get_dummies(data[dummy].astype(np.object_), prefix=dummy)
    data.drop(dummy, axis=1, inplace=True)
    #print(one_hot)
    data = data.join(one_hot)
print(data.head())
data.to_csv("onehot.csv", index=False)
